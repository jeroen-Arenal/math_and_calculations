using System;
using System.Collections.Generic;
using System.Text;

public class run
{
    public void init()
    {
        calibration calibrate =  new calibration();
        calibration.ReadFile();
        Console.WriteLine("init is done");             
    }

    public void calcultations ()
    {
        calculation calculate = new calculation();
                
        double casetemp = calculate.Casetemp(); 
       
        double TOF11 = calculate.TOF11(casetemp);
        double TOF12 = calculate.TOF12();
        double TOF13 = calculate.TOF13();
    
        double Power1m = calculate.Power1m(casetemp);
        double power2m = calculate.Power2m(casetemp);
        double power1A = calculate.Power1A(TOF11);

        double PT1000Temp = calculate.PT1000Temp();
        (double Tempusmodel, double Tempusmodel1) = calculate.TempUsModel(TOF11);
        (double UDTCw, double UDTdensityWater) = calculate.CoupleConstants(Tempusmodel,Tempusmodel1); 

        double SG = calculate.SG(Power1m,UDTdensityWater); 
        double Density = calculate.Density();  
        double TSS = calculate.TSS(UDTdensityWater);
        double Temperature = calculate.Temperature(Tempusmodel);

        /*
        double mA_out_1 = calculate.SelectmA1();
        double mA_out_2 = calculate.SelectmA2();
        double mA_out_3 = calculate.SelectmA3();
        double mA_out_4 = calculate.SelectmA4();
        double mA_out_5 = calculate.SelectmA5();
        double mA_out_6 = calculate.SelectmA6();
        */ 
    }
}
