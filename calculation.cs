using System;


public class calculation
{
    // modbus values
    public double UDTPT1000_x25 = 0;
    public double UDTPT1000_x26 = 0;
    public double UDTPT1000_x29 = 0;
    public double UDTPT1000_x30 = 0;


    // temporarily variables
    public double TempFP01 = 0;
    public double TempFP02 = 0;
    public double TempReal0 = 0;
    public double TempFP01X0 = 0;
    public double TempFP01X1 = 0;
    public double TempFP01X2 = 0;
    public double TempFP08 = 0;

    
    public double UDTTempUScorr = 0;

    // results power1a calculation 
    public double UDTPow1aAct = 0;
    public double UDTPow1aActCor =0;

    // results power1m calculation 
    public double UDTPow1mActRaw = 0;

    // results power2m calculation 
    public double UDTPow2mActRaw = 0; 

    // results PT1000 temp calculation 
    public double UDTTempPT1000act = 0;

    // counter
    public int i = 0;

    // temperature model results
    public double UDTTempUSmodel = 0;
    public double UDTTempUSmodel1 = 0;

    // button for aircalibration 
    public bool PeformAirCalibration = false;


    // number of UDT's
    public int Indirect1nr = 0;
    public int Indirect2nr = 0;

    // results specific gravity calculations
    public double SG_1_ResultFC = 0;
    public double SG_2_ResultFC = 0;
    public double UDTSGmodel = 0; 
    public double UDTSGcorr = 0;
    public double SG_1_ResultmA = 0;
    public double SG_2_ResultmA = 0;
    public double SG_A_ResultmA = 0;
    public double SG_AVG = 0;
    public double UDTDensityModel = 0;

    // results density calculations
    public double Density_1_ResultFC = 0;
    public double Density_2_ResultFC = 0;
    public double Density_1_ResultmA = 0;
    public double Density_2_ResultmA = 0;
    public double UDTDensitymodel = 0;
    public double Density_AVG = 0;
    public double Density_A_ResultmA = 0;
    public double Density_1_mA = 0;
    
    // results total suspended solids calculations
    public double TSS_1_ResultFC = 0;
    public double TSS_2_ResultFC = 0;
    public double TSS_1_ResultmA = 0;
    public double TSS_2_ResultmA = 0;
    public double TSS_AVG = 0;
    public double TSS_A_ResultmA = 0;
    public double TSS_1_mA = 0;
    public double UDTSolidsWtModel = 0;

    // results temperature calculations
    public double Temperature_1_ResultFC = 0;
    public double Temperature_2_ResultFC = 0;
    public double Temperature_1_ResultmA = 0;
    public double Temperature_2_ResultmA = 0;
    public double Temperature_AVG = 0;
    public double Temperature_A_ResultmA = 0;
    public double Temperature_1_mA = 0;


    
    public int LS2166 = 0;
    public int LS2167 = 0;
    public int USR01401 = 0;
    public int USR01403 = 0;
    public int USR01450 = 0;
    public int USR01453 = 0;
    public int USR01501 = 0;
    public int USR01503 = 0;
    public int USR01551 = 0;
    public int USR01553 = 0;
    public int USR02401 = 0;
    public int USR02403 = 0;
    public int USR02453 = 0;
    public int USR02501 = 0;
    public int USR02450 = 0;
    public int USR02503 = 0;
    public int USR02551 = 0;
    public int USR02553 = 0;
    public int USR01851 = 0;
    public int  USR0019715 = 0;


    // calibration values from calibration sheet
    public double AirCalibrationOffset = 0;
    public double UDTDensitySolids = 0;
    public double CalRiCoPow1mActint = 0;
    public double CalRiCoPow2mActint = 0;

    public double CalUDTCaseTempRefInt = 0;
    public double CalRiCoTOF11Int = 0;
    public double CalPow1aSG1 = 0;

    public double CalPow1a_1_MINTOFa = 0;
    public double CalPow1a_1_MINTOFb = 0;
    public double CalPow1a_2_MINTOFa = 0;
    public double CalPow1a_2_MINTOFb = 0;
    public double CalPow1a_3_MINTOFa = 0;
    public double CalPow1a_3_MINTOFb = 0;
    public double CalPow1a_4_MINTOFa = 0;
    public double CalPow1a_4_MINTOFb = 0;
    public double CalPow1a_5_MINTOFa = 0;
    public double CalPow1a_5_MINTOFb = 0;
    public double CalPow1a_6_MINTOFa = 0;
    public double CalPow1a_6_MINTOFb = 0;
        
    public double CalTempX00_dec = 0;
    public double CalTempX01_dec = 0;
    public double CalTempX02_dec = 0;
    public double CalTempX10_dec = 0;
    public double CalTempX11_dec = 0;
    public double CalTempX12_dec = 0;
    public double CalTempX20_dec = 0;
    public double CalTempX21_dec = 0;
    public double CalTempX22_dec = 0;
    public double CalTempX0_exp = 0;
    public double CalTempX0_real = 0;
    public double CalTempX1_exp = 0;
    public double CalTempX1_real = 0;
    public double CalTempX2_exp = 0;
    public double CalTempX2_real = 0;

    
    public double CalPow1a_1aA_dec = 0;
    public double CalPow1a_1aB_dec = 0;
    public double CalPow1a_1aC_dec = 0;
    public double CalPow1a_1bA_dec = 0;
    public double CalPow1a_1bB_dec = 0;
    public double CalPow1a_1bC_dec = 0;
    public double CalPow1a_1cA_dec = 0;
    public double CalPow1a_1cB_dec = 0;
    public double CalPow1a_1cC_dec = 0;

    public double CalPow1a_2aA_dec = 0;
    public double CalPow1a_2aB_dec = 0;
    public double CalPow1a_2aC_dec = 0;
    public double CalPow1a_2bA_dec = 0;
    public double CalPow1a_2bB_dec = 0;
    public double CalPow1a_2bC_dec = 0;
    public double CalPow1a_2cA_dec = 0;
    public double CalPow1a_2cB_dec = 0;
    public double CalPow1a_2cC_dec = 0;

    public double CalPow1a_3aA_dec = 0;
    public double CalPow1a_3aB_dec = 0;
    public double CalPow1a_3aC_dec = 0;
    public double CalPow1a_3bA_dec = 0;
    public double CalPow1a_3bB_dec = 0;
    public double CalPow1a_3bC_dec = 0;
    public double CalPow1a_3cA_dec = 0;
    public double CalPow1a_3cB_dec = 0;
    public double CalPow1a_3cC_dec = 0;

    public double CalPow1a_4aA_dec = 0;
    public double CalPow1a_4aB_dec = 0;
    public double CalPow1a_4aC_dec = 0;
    public double CalPow1a_4bA_dec = 0;
    public double CalPow1a_4bB_dec = 0;
    public double CalPow1a_4bC_dec = 0;
    public double CalPow1a_4cA_dec = 0;
    public double CalPow1a_4cB_dec = 0;
    public double CalPow1a_4cC_dec = 0;

    public double CalPow1a_5aA_dec = 0;
    public double CalPow1a_5aB_dec = 0;
    public double CalPow1a_5aC_dec = 0;
    public double CalPow1a_5bA_dec = 0;
    public double CalPow1a_5bB_dec = 0;
    public double CalPow1a_5bC_dec = 0;
    public double CalPow1a_5cA_dec = 0;
    public double CalPow1a_5cB_dec = 0;
    public double CalPow1a_5cC_dec = 0;

    public double CalPow1a_6aA_dec = 0;
    public double CalPow1a_6aB_dec = 0;
    public double CalPow1a_6aC_dec = 0;
    public double CalPow1a_6bA_dec = 0;
    public double CalPow1a_6bB_dec = 0;
    public double CalPow1a_6bC_dec = 0;
    public double CalPow1a_6cA_dec = 0;
    public double CalPow1a_6cB_dec = 0;
    public double CalPow1a_6cC_dec = 0;

    public double CalPow1a_1a_real = 0;
    public double CalPow1a_1b_real = 0;
    public double CalPow1a_1c_real = 0;

    public double CalPow1a_2a_real = 0;
    public double CalPow1a_2b_real = 0;
    public double CalPow1a_2c_real = 0;

    public double CalPow1a_3a_real = 0;
    public double CalPow1a_3b_real = 0;
    public double CalPow1a_3c_real = 0;

    public double CalPow1a_4a_real = 0;
    public double CalPow1a_4b_real = 0;
    public double CalPow1a_4c_real = 0;

    public double CalPow1a_5a_real = 0;
    public double CalPow1a_5b_real = 0;
    public double CalPow1a_5c_real = 0;

    public double CalPow1a_6a_real = 0;
    public double CalPow1a_6b_real = 0;
    public double CalPow1a_6c_real = 0;


    public int SG_1_D1 = 1;
    public int SG_1_D2 = 2;
    public int SG_1_D3 = 1;
    public int SG_1_D4 = 2;
    public int SG_1_e0 = 1;
    public int SG_1_e1 = 2;
    public int SG_1_e2 = 0;
    public int SG_1_e3 = 2;
    public int SG_1_e4 = 0;
    public int SG_1_e5 = 3;
    public int Density_1_d1 = 1000;
    public int Density_1_d2 = 2000;
    public int Density_1_d3 = 1000;
    public int Density_1_d4 = 2000;
    public int Density_1_e0 = 1000;
    public int Density_1_e1 = 2000;
    public int Density_1_e2 = 900;
    public int Density_1_e3 = 2500;
    public int Density_1_e4 = 500;
    public int Density_1_e5 = 3400;
    public int TSS_1_d1 = 0;
    public int TSS_1_d2 = 100;
    public int TSS_1_d3 = 0;
    public int TSS_1_d4 = 100;
    public int TSS_1_e0 = 0;
    public int TSS_1_e1 = 100;
    public int TSS_1_e2 = 0;
    public int TSS_1_e3 = 100;
    public int TSS_1_e4 = 0;
    public int TSS_1_e5 = 100;
    public int Temperature_1_d1 = 5;
    public int Temperature_1_d2 = 50;
    public int Temperature_1_d3 = 5;
    public int Temperature_1_d4 = 50;
    public int Temperature_1_e0 = 5;
    public int Temperature_1_e1 = 50;
    public int Temperature_1_e2 = 3;
    public int Temperature_1_e3 = 60;
    public int Temperature_1_e4 = 1 ;
    public int Temperature_1_e5 = 65;

   


    public double Casetemp () 
    {
        //calculations casetemp 
        var UDTCaseTemp =  ((((((UDTPT1000_x30 * 65536) + UDTPT1000_x29) - 8388608)*1.17)/6794.772) - 273.15) ;
        var UDTCaseTempdif = (CalUDTCaseTempRefInt/10) - UDTCaseTemp;
        Console.WriteLine("casetemp = "+UDTCaseTempdif);

        return UDTCaseTempdif;
    }
    public double TOF11(double casetempdif) 
    {
        //get time of flight from RS485
        {
            return 0;
        }

    }

    public double TOF12() 
    {
         //get time of flight from RS485
        {
            return 0;
        }
        
    }

    public double TOF13 () 
    {
        //get time of flight from RS485
        {
            return 0;
        } 

    }
    public double Power1m (double CaseTempdif) 
    {
        var power1m = (65536 * 1/*INPUT dwtoreal(UDTPow1fraction,UDTPOW1dec)*/)+ (/*INPUT*/1/65535.0)/1000;
        int UDTAttenuator1 = 3;

        switch (UDTAttenuator1)
        {

            case 1:  
                UDTPow1mActRaw = power1m * 2.606201; 
            break;

            case 2:  
                UDTPow1mActRaw = power1m * 1.300049;
            break;

            case 3:  
                UDTPow1mActRaw = power1m * 0.6530762; 
            break;

            case 4:  
                UDTPow1mActRaw = power1m * 0.3173828; 
            break;

            case 5:  
                UDTPow1mActRaw = power1m * 0.1647949; 
            break;

            case 6:  
                UDTPow1mActRaw = power1m * 0.0793457; 
            break;

            case 7:  
                UDTPow1mActRaw = power1m * 0.04272461; 
            break;

            case 8:  
                UDTPow1mActRaw = power1m * 0.01831055; 
            break;

            case 0:    
                UDTPow1mActRaw = power1m;
            break;

        }
        var UDTPow1mActCor = (CalRiCoPow1mActint/10000) * CaseTempdif + UDTPow1mActRaw;
        Console.WriteLine("POWER1m = "+UDTPow1mActCor);

        return UDTPow1mActCor;   
    }
    public double Power2m (double CaseTempdif) 
    {
        var power2m = (65536 * 1/*INPUT dwtoreal(UDTPow1fraction,UDTPOW1dec)*/)+ (/*INPUT*/1/65535.0)/1000;
        int UDTAttenuator2 = 4;

        switch (UDTAttenuator2)
        {

            case 1:  
                UDTPow2mActRaw = power2m * 2.606201; 
            break;

            case 2:  
                UDTPow2mActRaw = power2m * 1.300049;
            break;

            case 3:  
                UDTPow2mActRaw = power2m * 0.6530762; 
            break;

            case 4:  
                UDTPow2mActRaw = power2m * 0.3173828; 
            break;

            case 5:  
                UDTPow2mActRaw = power2m * 0.1647949; 
            break;

            case 6:  
                UDTPow2mActRaw = power2m * 0.0793457; 
            break;

            case 7:  
                UDTPow2mActRaw = power2m * 0.04272461; 
            break;

            case 8:  
                UDTPow2mActRaw = power2m * 0.01831055; 
            break;

            case 0:    
                UDTPow2mActRaw = power2m;
            break;

        }
        var UDTPow2mActCor = (CalRiCoPow2mActint/10000) * CaseTempdif + UDTPow2mActRaw;

        Console.WriteLine("POWER2m = "+UDTPow2mActCor);
        return UDTPow2mActCor;
        
    }
    public double Power1A (double UDTTOF11actCor)
    {
        int USR01809 = 0;
        switch (i)
        {

            case 0:  
                
                TempFP01 = CalPow1a_1_MINTOFa + (CalPow1a_1_MINTOFb/10000);

                if (UDTTOF11actCor >= TempFP01)
                { 
                    USR01809 = 1;
                }  
                else
                { 
                    i++;
                }
        
            break;
                
            case 1:  

                TempFP01 = CalPow1a_2_MINTOFa + (CalPow1a_2_MINTOFb/10000);

                 if (UDTTOF11actCor >= TempFP01)
                { 
                    USR01809 = 2;
                }  
                else
                { 
                    i++;
                }
       
            break;

            case 2:  
            
                TempFP01 = CalPow1a_3_MINTOFa + (CalPow1a_3_MINTOFb/10000);

                if (UDTTOF11actCor >= TempFP01)
                { 
                    USR01809 = 3;
                }  
                else
                { 
                    i++;
                }
         
            break;  
                
            case 3:  
            
                TempFP01 = CalPow1a_4_MINTOFa + (CalPow1a_4_MINTOFb/10000);

                if (UDTTOF11actCor >= TempFP01)
                { 
                    USR01809 = 4;
                }  
                else
                { 
                    i++;
                }

            break;
           
            case 4:  

                TempFP01 = CalPow1a_5_MINTOFa + (CalPow1a_5_MINTOFb/10000);

                if (UDTTOF11actCor >= TempFP01)
                { 
                    USR01809 = 5;
                }  
                else
                { 
                    i++;                    
                }

            break;

            case 5:  
    
                TempFP01 = CalPow1a_6_MINTOFa + (CalPow1a_6_MINTOFb/10000);

                if (UDTTOF11actCor >= TempFP01)
                { 
                    USR01809 = 6;
                }  
                else
                { 
                    i = 0;
                }

            break;
        } 

        switch (USR01809)
        {
            case 1: 
                
                if (CalPow1a_1aA_dec >= 0)
                {
                    TempFP01 = CalPow1a_1aA_dec + (CalPow1a_1aB_dec/1000000000);
                }
                else 
                {
                    TempFP01 = CalPow1a_1aA_dec - (CalPow1a_1aB_dec/1000000000);
                }    
                
                CalPow1a_1a_real = TempFP01 / (Math.Pow(10,CalPow1a_1aC_dec));
                            
                if (CalPow1a_1bA_dec >= 0)
                {
                    TempFP01 = CalPow1a_1bA_dec + (CalPow1a_1bB_dec/1000000000);
                }
                else
                { 
                    TempFP01 = CalPow1a_1bA_dec - (CalPow1a_1bB_dec/1000000000);
                }   

                CalPow1a_1b_real = TempFP01 / (Math.Pow(10,CalPow1a_1bC_dec));
                                
                if (CalPow1a_1cA_dec >= 0)
                {
                    TempFP01 = CalPow1a_1cA_dec + (CalPow1a_1cB_dec/1000000000);
                }
                
                else
                {                
                    TempFP01 = CalPow1a_1cA_dec - (CalPow1a_1cB_dec/1000000000);
                }

                CalPow1a_1c_real = TempFP01 / (Math.Pow(10,CalPow1a_1cC_dec));
                UDTPow1aAct = (Math.Pow(UDTTOF11actCor,2)) * CalPow1a_1a_real + (CalPow1a_1b_real * UDTTOF11actCor) + CalPow1a_1c_real;

            break; 

            case 2:

                if (CalPow1a_2aA_dec >= 0)
                {
                    TempFP01 = CalPow1a_2aA_dec + (CalPow1a_2aB_dec/1000000000);
                }   

                else
                {                
                    TempFP01 = CalPow1a_2aA_dec - (CalPow1a_2aB_dec/1000000000);
                }
                    
                CalPow1a_2a_real = TempFP01 / (Math.Pow(10,CalPow1a_2aC_dec));
                    
                if (CalPow1a_2bA_dec >= 0)
                {
                    TempFP01 = CalPow1a_2bA_dec + (CalPow1a_2bB_dec/1000000000);
                }
                else
                {
                    TempFP01 = CalPow1a_2bA_dec - (CalPow1a_2bB_dec/1000000000);
                }
                
                CalPow1a_2b_real = TempFP01 / (Math.Pow(10,CalPow1a_2bC_dec));
                    
                if (CalPow1a_2cA_dec >= 0)
                {
                    TempFP01 = CalPow1a_2cA_dec + (CalPow1a_2cB_dec/1000000000);
                }
                else
                {  
                    TempFP01 = CalPow1a_2cA_dec - (CalPow1a_2cB_dec/1000000000);
                }
                
                CalPow1a_2c_real = TempFP01 / (Math.Pow(10,CalPow1a_2cC_dec));            
                UDTPow1aAct = Math.Pow(UDTTOF11actCor,2) * CalPow1a_2a_real + (CalPow1a_2b_real * UDTTOF11actCor) + CalPow1a_2c_real;
                
            break;

            case 3: 
            
                if (CalPow1a_3aA_dec >= 0)
                {
                    TempFP01 = CalPow1a_3aA_dec + (CalPow1a_3aB_dec/1000000000);
                }
                else
                {
                    TempFP01 = CalPow1a_3aA_dec - (CalPow1a_3aB_dec/1000000000);
                } 

                CalPow1a_3a_real = TempFP01 / (Math.Pow(10,CalPow1a_3aC_dec));
                        
                if (CalPow1a_3bA_dec >= 0)
                {
                    TempFP01 = CalPow1a_3bA_dec + (CalPow1a_3bB_dec/1000000000);
                }
                else
                {
                    TempFP01 = CalPow1a_3bA_dec - (CalPow1a_3bB_dec/1000000000);
                }                
                
                CalPow1a_3b_real = TempFP01 / (Math.Pow(10,CalPow1a_3bC_dec));
                                
                if (CalPow1a_3cA_dec >= 0)
                {
                    TempFP01 = CalPow1a_3cA_dec + (CalPow1a_3cB_dec/1000000000);
                }
                else
                {   
                    TempFP01 = CalPow1a_3cA_dec - (CalPow1a_3cB_dec/1000000000);
                }
                        
                CalPow1a_3c_real = TempFP01 / (Math.Pow(10,CalPow1a_3cC_dec));    
                UDTPow1aAct = Math.Pow(UDTTOF11actCor,2) * CalPow1a_3a_real + (CalPow1a_3b_real * UDTTOF11actCor) + CalPow1a_3c_real ;
                
            break;    

            case 4: 
            
                if (CalPow1a_4aA_dec >= 0)
                {
                    TempFP01 = CalPow1a_4aA_dec + (CalPow1a_4aB_dec/1000000000);
                }
                else
                { 
                    TempFP01 = CalPow1a_4aA_dec - (CalPow1a_4aB_dec/1000000000);
                }    
                
                CalPow1a_4a_real = TempFP01 / (Math.Pow(10,CalPow1a_4aC_dec));
                
                if (CalPow1a_4bA_dec >= 0)
                {
                    TempFP01 = CalPow1a_4bA_dec + (CalPow1a_4bB_dec/1000000000);
                }
                else
                {   
                    TempFP01 = CalPow1a_4bA_dec - (CalPow1a_4bB_dec/1000000000);
                }    
            
                CalPow1a_4b_real = TempFP01 / (Math.Pow(10,CalPow1a_4bC_dec));
                    
                if (CalPow1a_4cA_dec >= 0)
                { 
                    TempFP01 = CalPow1a_4cA_dec + (CalPow1a_4cB_dec/1000000000);
                }
                else
                {  
                    TempFP01 = CalPow1a_4cA_dec - (CalPow1a_4cB_dec/1000000000);
                }    

                CalPow1a_4c_real = TempFP01 / (Math.Pow(10,CalPow1a_4cC_dec));
                UDTPow1aAct = Math.Pow(UDTTOF11actCor,2) * CalPow1a_4a_real + (CalPow1a_4b_real * UDTTOF11actCor) + CalPow1a_4c_real;
                
            break;
                
            case 5: 
            
                if (CalPow1a_5aA_dec >= 0)
                {
                    TempFP01 = CalPow1a_5aA_dec + (CalPow1a_5aB_dec/1000000000);
                }
                else
                { 
                    TempFP01 = CalPow1a_5aA_dec - (CalPow1a_5aB_dec/1000000000);
                }    
            
                CalPow1a_5a_real = TempFP01 / (Math.Pow(10,CalPow1a_5aC_dec));
                    
                if (CalPow1a_5bA_dec >= 0)
                { 
                    TempFP01 = CalPow1a_5bA_dec + (CalPow1a_5bB_dec/1000000000);
                }
                else
                { 
                    TempFP01 = CalPow1a_5bA_dec - (CalPow1a_5bB_dec/1000000000);
                }

                CalPow1a_5b_real = TempFP01 / (Math.Pow(10,CalPow1a_5bC_dec));
                    
                if (CalPow1a_5cA_dec >= 0)
                {
                    TempFP01 = CalPow1a_5cA_dec + (CalPow1a_5cB_dec/1000000000);
                }
                else
                {  
                    TempFP01 = CalPow1a_5cA_dec - (CalPow1a_5cB_dec/1000000000);
                }

                CalPow1a_5c_real = TempFP01 / (Math.Pow(10,CalPow1a_5cC_dec));
                UDTPow1aAct = Math.Pow(UDTTOF11actCor,2) * CalPow1a_5a_real + (CalPow1a_5b_real * UDTTOF11actCor) + CalPow1a_5c_real;
                
            break;
                
            case 6:

                if (CalPow1a_6aA_dec >= 0)
                {
                    TempFP01 = CalPow1a_6aA_dec + (CalPow1a_6aB_dec/1000000000);
                }
                else
                {   
                    TempFP01 = CalPow1a_6aA_dec - (CalPow1a_6aB_dec/1000000000);
                }    
            
                CalPow1a_6a_real = TempFP01 / (Math.Pow(10,CalPow1a_6aC_dec));
                    
                if (CalPow1a_6bA_dec >= 0)
                {
                    TempFP01 = CalPow1a_6bA_dec + (CalPow1a_6bB_dec/1000000000);
                }
                else
                {  
                    TempFP01 = CalPow1a_6bA_dec - (CalPow1a_6bB_dec/1000000000);
                }

                CalPow1a_6b_real = TempFP01 / (Math.Pow(10,CalPow1a_6bC_dec));
                    
                if (CalPow1a_6cA_dec >= 0)
                {
                    TempFP01 = CalPow1a_6cA_dec + (CalPow1a_6cB_dec/1000000000);
                }
                else
                { 
                    TempFP01 = CalPow1a_6cA_dec - (CalPow1a_6cB_dec/1000000000);
                }    
            
                CalPow1a_6c_real = TempFP01 / (Math.Pow(10,CalPow1a_6cC_dec));
                UDTPow1aAct = Math.Pow(UDTTOF11actCor,2) * CalPow1a_6a_real + (CalPow1a_6b_real * UDTTOF11actCor) + CalPow1a_6c_real;
    
            break;
        }

        var UDTPow1aAct1 = UDTPow1aAct + (AirCalibrationOffset/100);

        if (PeformAirCalibration)
        {
            AirCalibrationOffset = (UDTPow1aActCor - (UDTPow1aAct1 - (AirCalibrationOffset/100))) * 100;
            PeformAirCalibration = false;
        }
        return UDTPow1aAct1;         
    }

    public double PT1000Temp()
    {
        UDTTempPT1000act = ((Math.Sqrt(((((UDTPT1000_x26*65536) + UDTPT1000_x25)*0.000178814)-1000)-500)*(-0.00231)+17.58481) - 3.9083)/-0.001155;

        return UDTTempPT1000act;
    }
    public (double, double)TempUsModel (double UDTTOF11actCor)
    {
              
        if (CalTempX00_dec >= 0)
        {
            TempReal0 = CalTempX00_dec + (CalTempX01_dec/1000000000);
        }
        else
        { 
            TempReal0 = CalTempX00_dec - (CalTempX01_dec/1000000000);
        }

        TempFP01X0 = CalTempX0_exp;
        CalTempX0_real = TempReal0 / (Math.Pow(10,TempFP01X0));  

        if (CalTempX10_dec >= 0)
        { 
            TempReal0 = CalTempX10_dec + (CalTempX11_dec/1000000000);
        }
        else
        {
            TempReal0 = CalTempX10_dec - (CalTempX11_dec/1000000000);
        }
           
        TempFP01X1 = CalTempX1_exp;
        CalTempX1_real = TempReal0 /(Math.Pow(10,TempFP01X1));

        if (CalTempX20_dec >= 0)
        {
            TempReal0 = CalTempX20_dec + (CalTempX21_dec/1000000000);
        }
        else
        { 
            TempReal0 = CalTempX20_dec - (CalTempX21_dec/1000000000);
        }

        TempFP01X2 = CalTempX2_exp;
        CalTempX2_real = TempReal0 / (Math.Pow(10,TempFP01X2));

        UDTTempUSmodel = (CalTempX1_real * UDTTOF11actCor) + CalTempX2_real + (CalTempX0_real * Math.Pow(UDTTOF11actCor,2));
        UDTTempUSmodel1 = (CalTempX1_real * UDTTOF11actCor) + CalTempX2_real + (CalTempX0_real * Math.Pow(UDTTOF11actCor,2)) + UDTTempUScorr;
        
        return (UDTTempUSmodel,UDTTempUSmodel1);
    }

    public (double, double) CoupleConstants(double UDTTempUSmodel,double UDTTempUSmodel1)
    {

     var UDTCw = 1402.405 + (Math.Pow(UDTTempUSmodel,4.0) * (-1.330291*Math.Pow(10.0,-6.0))) + (Math.Pow(UDTTempUSmodel,5.0) * (2.198696*Math.Pow(10.0,-9.0))) + ((Math.Pow(UDTTempUSmodel,3.0) * 0.000325373)) + (Math.Pow(UDTTempUSmodel,2.0) * (-0.05783388)) + (UDTTempUSmodel1 * 5.033709);
     var UDTDensityWater = 999.8426 + (Math.Pow(UDTTempUSmodel,4.0) * (-1.121157*Math.Pow(10.0,-6.0))) + (Math.Pow(UDTTempUSmodel,5.0) * (6.54701*Math.Pow(10.0,-9.0))) + (Math.Pow(UDTTempUSmodel,3.0) * (0.0001002066)) + (Math.Pow(UDTTempUSmodel,2.0) * (-0.009095845)) + (UDTTempUSmodel1 * 0.06794234);
    
     return(UDTCw,UDTDensityWater);
    }

    public double SG(double UDTPow1mActCor, double UDTDensityWater)
    {

        if (USR0019715 == 1.0)
        { 
            UDTSGcorr = USR01851/1000;
        }
        else
        {
            UDTSGcorr = USR01851/1000;
        }

        if ((((UDTPow1mActCor - UDTPow1aAct) / CalPow1aSG1) + UDTSGcorr) < 0)
        {
            UDTSGmodel = 0.0;
        }
        else 
        {
            UDTSGmodel = (((UDTPow1mActCor - UDTPow1aAct) / CalPow1aSG1) + UDTSGcorr);
        }
        
        if (Indirect1nr == 1) 
        {
            TempFP08 = SG_1_ResultFC;
        }
        else if (Indirect1nr == 2)
        {
            TempFP08 = SG_2_ResultFC;
        }

        if (Indirect1nr == 1)
        {
            SG_1_ResultFC = (((SG_1_D3 - SG_1_D4)/(SG_1_D1 - SG_1_D2)) * UDTSGmodel) + ((((SG_1_D3 - SG_1_D4)/(SG_1_D1 - SG_1_D2)) * SG_1_D1) - SG_1_D3);
        }
        else if (Indirect1nr == 2)
        {
            SG_2_ResultFC = (((SG_1_D3 - SG_1_D4)/(SG_1_D1 - SG_1_D2)) * UDTSGmodel) + ((((SG_1_D3 - SG_1_D4)/(SG_1_D1 - SG_1_D2)) * SG_1_D1) - SG_1_D3); 
        }
        
        var USR01401 = 5.0;

        if (Indirect1nr == 1)
        {
            if (USR01401 > 1.0)
            {    
                SG_1_ResultFC = (SG_1_ResultFC * (1.0 / USR01401)) + (TempFP08 * (1.0 - (1.0 /USR01401)));
            } 
        }

        else if (Indirect1nr == 2)
        {

            if (USR02401 > 1.0)
            {
                SG_2_ResultFC = (SG_2_ResultFC * (1.0 / USR02401)) + (TempFP08 * (1.0 - (1.0 /USR02401)));
            }
        }

        if (Indirect1nr == 1)
        {
            TempFP01 = (SG_1_ResultFC - SG_1_e0) / (SG_1_e1 - SG_1_e0);

            if (TempFP01 <= 0.0)
            {        
                SG_1_ResultmA = 4.0;
            }
            else if (TempFP01 > 0.0 && TempFP01 <= 1.0) 
            {
                SG_1_ResultmA = (TempFP01 * 16.0) + 4.0; 
            }
            else if (TempFP01 > 1.0)
            {
                SG_1_ResultmA = 20.0;
            }       
        }

        else if (Indirect1nr == 2) 
        {
            TempFP01 = (SG_2_ResultFC - SG_1_e0) / (SG_1_e1 - SG_1_e0);

            if (TempFP01 <= 0.0)
            {
                SG_2_ResultmA = 4.0;
            }
            else if (TempFP01 > 0.0 && TempFP01 <= 1.0)
            {        
                SG_2_ResultmA = (TempFP01 * 16) + 4; 
            }
            else if (TempFP01 > 1.0) 
            {
                SG_2_ResultmA = 20.0;
            }
        }

        SG_AVG = (SG_1_ResultFC + SG_2_ResultFC) / 2.0;
        SG_A_ResultmA = (SG_1_ResultmA + SG_2_ResultmA) / 2.0;
         
        if (Indirect1nr == 1)
        {
            if (SG_1_ResultFC < SG_1_e4 || SG_1_ResultFC > SG_1_e5) 
            {
                USR01403 = 16;
            }
        }          
        else if (Indirect1nr == 2)
        {
            if (SG_1_ResultFC < SG_1_e4 || SG_1_ResultFC > SG_1_e5)
            {
                USR02403 = 16;
            } 
        }          

        if (Indirect1nr == 1)
        {
            if (SG_1_ResultFC < SG_1_e2)
            {   
                USR01403 = 4;
                LS2166 = 36866;
                LS2167 = 2005;          
            }
        }          
        else if (Indirect1nr == 2) 
        {   
            if (SG_1_ResultFC < SG_1_e2)  
            {   
                USR02403 = 4;
                LS2166 = 36866;
                LS2167 = 2005;
            }  
        }   

        if (Indirect1nr == 1)
        {
            if (SG_1_ResultFC >SG_1_e3)  
            {   
                USR01403 = 8;
                LS2166 = 36866;
                LS2167 = 2006;
            }             
        }        
        else if (Indirect1nr == 2) 
        {
            if (SG_1_ResultFC > SG_1_e3) 
            {   
                USR02403 = 8;
                LS2166 = 36866;
                LS2167 = 2006;
            }  
        }     

        if (Indirect1nr == 1)
        {
            TempFP08 = Density_1_ResultFC;
        }       
        else if (Indirect1nr == 2)
        {
            TempFP08 = Density_2_ResultFC;
        }        
         
        if (Indirect1nr == 1) 
        {
            if (USR01450 == 0) 
            {
                UDTDensityModel = UDTSGmodel * UDTDensityWater;
            }
            else if (USR01450 == 1)
            {
                UDTDensityModel = SG_1_ResultFC * UDTDensityWater;
            }
            else if (USR01450 == 2) 
            {
                UDTDensityModel = SG_1_ResultFC * 1000;
            }              
        }        
        else if (Indirect1nr == 2) 
        {    
            if (USR02450 == 0) 
            {
                UDTDensityModel = UDTSGmodel * UDTDensityWater;
            }
            else if (USR02450 == 1)
            {
                UDTDensityModel = SG_2_ResultFC * UDTDensityWater;
            }
            else if (USR02450 == 2)
            {
                UDTDensityModel = SG_2_ResultFC * 1000;
            }    
        }

        return (UDTDensitymodel);   
    }
    public double Density ()
    {
        if (Indirect1nr == 1)
        {
            Density_1_ResultFC = (((Density_1_d3 - Density_1_d4)/(Density_1_d1 - Density_1_d2)) * UDTDensityModel) + ((((Density_1_d3 - Density_1_d4)/(Density_1_d1 - Density_1_d2)) * Density_1_d1) - Density_1_d3);
        }   
        else if (Indirect1nr == 2)
        {
            Density_2_ResultFC = (((Density_1_d3 - Density_1_d4)/(Density_1_d1 - Density_1_d2)) * UDTDensityModel) + ((((Density_1_d3 - Density_1_d4)/(Density_1_d1 - Density_1_d2)) * Density_1_d1) - Density_1_d3);
        }

        if (Indirect1nr == 1) 
        {
            if (USR01401 > 1) 
            {
                Density_1_ResultFC = (Density_1_ResultFC * (1.0 /USR01401)) + (TempFP08 * (1.0 - (1.0 / USR01401)));
            }
        }	
        else if (Indirect1nr == 2)
        {
            if (USR02401 > 1.0) 
            {
                Density_2_ResultFC = (Density_2_ResultFC * (1.0 / USR01401)) + (TempFP08 * (1.0 - (1.0 / USR01401)));
            }
        }	

        if (Indirect1nr == 1)
        {
            TempFP01 = (Density_1_ResultFC - Density_1_e0) / (Density_1_e1 - Density_1_e0);
        
            if (TempFP01 <= 0.0)
            {
                Density_1_ResultmA = 4;
            }
            else if (TempFP01 > 0.0 && TempFP01 <= 1.0)
            {
                Density_1_ResultmA = (TempFP01 * 16) + 4; 
            }
            else if (TempFP01 > 1.0)
            {
                Density_1_ResultmA = 20;
            }     
        }  
        else if (Indirect1nr == 2)       
        {
            TempFP01 = (Density_2_ResultFC - Density_1_e0) / (Density_1_e1 - Density_1_e0);
        
            if (TempFP01 <= 0.0)
            {
                Density_2_ResultmA = 4.0;
            }     
            else if (TempFP01 > 0.0 && TempFP01 <= 1.0)
            {
                Density_2_ResultmA = (TempFP01 * 16) + 4; 
            }
            else if (TempFP01 > 1.0)
            {
                Density_2_ResultmA = 20.0;
            }
        }

        Density_AVG = (Density_1_ResultFC + Density_2_ResultFC) / 2;
        Density_A_ResultmA = (Density_1_mA + Density_1_mA) / 2; 

        if (Indirect1nr == 1)
        {
            if (Density_1_ResultFC < Density_1_e4 || Density_1_ResultFC > Density_1_e5) 
            {       
                USR01453 = 16;
            }
        }   
        else if (Indirect1nr == 2)
        {
            if (Density_1_ResultFC < Density_1_e4 || Density_1_ResultFC > Density_1_e5)
            {
        
                USR02453 = 16;
            }
        }            
        
        if (Indirect1nr == 1)
        { 
            if (Density_1_ResultFC < Density_1_e2)
            {
                USR01453 = 4;
                LS2166 = 36867;
                LS2167 = 2005;
            }
            else if (Density_1_ResultFC > Density_1_e3) 
            {
                USR01453 = 8;
                LS2166 = 36867;
                LS2167 = 2006;
            }
        }    
        else if (Indirect1nr == 2)
        {
            if (Density_2_ResultFC < Density_1_e2) 
            {
                USR01453 = 4;
                LS2166 = 36867;
                LS2167 = 2005;
            }
            else if (Density_2_ResultFC > Density_1_e3)
            {
                USR01453 = 8;
                LS2166 = 36867;
                LS2167 = 2006;
            }
        }

        return (0.0);
    }
    public double TSS(double UDTDensityWater)
    {
        if (Indirect1nr == 1) 
        {
            TempFP08 = TSS_1_ResultFC;
        }
        else if (Indirect1nr == 2)
        {
            TempFP08 = TSS_2_ResultFC;
        }
        
        if (UDTDensityModel > UDTDensityWater)
        {
            UDTSolidsWtModel = (((UDTDensityModel - UDTDensityWater) * UDTDensitySolids)/((UDTDensitySolids - UDTDensityWater) * UDTDensityModel))*100;
        }
        else
        { 
            UDTSolidsWtModel = 0.0;
        }
        
        TempFP02 = TSS_1_d3 - (((TSS_1_d3 - TSS_1_d4) / (TSS_1_d1 - TSS_1_d2)) * TSS_1_d1);
        TempFP01 = (((TSS_1_d3 - TSS_1_d4) / (TSS_1_d1 - TSS_1_d2)) * UDTSolidsWtModel);

        if (Indirect1nr == 1)
        {
            TSS_1_ResultFC = TempFP01 + TempFP02;
        }
        else if (Indirect1nr == 2)
        {
            TSS_2_ResultFC = TempFP01 + TempFP02;
        }

        if (Indirect1nr == 1)
        {
            if (USR01501 > 1)
            { 
                TSS_1_ResultFC = (TSS_1_ResultFC * (1 /USR01551)) + (TempFP08 * (1 - (1 / USR01551)));
            }
        }
        else if (Indirect1nr == 2)
        {
            if (USR02501 > 1)
            {
                TSS_2_ResultFC = (TSS_2_ResultFC * (1 / USR01551)) + (TempFP08 * (1 - (1 / USR01551)));
            }
        }

        if (Indirect1nr == 1) 
        {
            TempFP01 = (TSS_1_ResultFC - TSS_1_e0) / (TSS_1_e1 - TSS_1_e0);

            if (TempFP01 <= 0)
            {
                TSS_1_ResultmA = 4.0;
            }
            
            else if (TempFP01 > 0 && TempFP01 <= 1)
            {
                TSS_1_ResultmA = (TempFP01 * 16) + 4;
            } 
            
            else if (TempFP01 > 1 )
            {
                TSS_1_ResultmA = 20.0;
            }
        }                
        else if (Indirect1nr == 2) 
        {
            TempFP01 = (TSS_2_ResultFC - TSS_1_e0) / (TSS_1_e1 - TSS_1_e0);
        
            if (TempFP01 <= 0.0)
            {    
                TSS_2_ResultmA = 4.0;
            }
            else if (TempFP01 > 0 && TempFP01 <= 1) 
            {
                TSS_2_ResultmA = (TempFP01 * 16) + 4; 
            }
            else if (TempFP01 > 1)             
            {
                TSS_2_ResultmA = 20.0;
            }
        }

        TSS_AVG = (TSS_1_ResultFC + TSS_2_ResultFC) / 2;
        TSS_A_ResultmA = (TSS_1_mA + TSS_1_mA) / 2; 

        if (Indirect1nr == 1)
        {
            if (TSS_1_ResultFC < TSS_1_e4 || TSS_1_ResultFC > TSS_1_e5) 
            {
                USR01503 = 16;
            }     
        }        
        else if (Indirect1nr == 2) 
        {
            if (TSS_1_ResultFC < TSS_1_e4 || TSS_1_ResultFC > TSS_1_e5) 
            {
                USR02503 = 16;
            }    
        } 
        if (Indirect1nr == 1)
        {
            if (TSS_1_ResultFC < TSS_1_e2) 
            {
                USR01453 = 4;
                LS2166 = 36868;
                LS2167 = 2005;
            }
            else if (TSS_1_ResultFC > TSS_1_e3) 
            {
                USR01453 = 8;
                LS2166 = 36868;
                LS2167 = 2006;
            }
        }   
        else if (Indirect1nr == 2)
        {
            if (TSS_2_ResultFC < TSS_1_e2) 
            {
                USR01453 = 4;
                LS2166 = 36868;
                LS2167 = 2005;
            }
        
            else if (TSS_2_ResultFC > TSS_1_e3) 
            {
            USR01453 = 8;
            LS2166 = 36868;
            LS2167 = 2006;
            }  
        }
        return(0);
    }
    public double Temperature(double UDTTempUSmodel)
    {
        if (Indirect1nr == 1) 
        {
            TempFP08 = Temperature_1_ResultFC;
        }
        else if (Indirect1nr == 2) 
        {
            TempFP08 = Temperature_2_ResultFC;
        }
 
        TempFP02 = Temperature_1_d3 - (((Temperature_1_d3 - Temperature_1_d4) / (Temperature_1_d1 - Temperature_1_d2)) * Temperature_1_d1);
        TempFP01 = (((Temperature_1_d3 - Temperature_1_d4) / (Temperature_1_d1 - Temperature_1_d2)) * UDTTempUSmodel);

        if (Indirect1nr == 1) 
        {
            Temperature_1_ResultFC = TempFP01 + TempFP02;
        }
        else if (Indirect1nr == 2)
        {
            Temperature_2_ResultFC = TempFP01 + TempFP02;
        }
        
        if (Indirect1nr == 1)
        {
            if (USR01551 > 1)
            {
                Temperature_1_ResultFC = (Temperature_1_ResultFC * (1.0 / USR01551)) + (TempFP08 * (1.0 - (1.0 / USR01551)));
            }
        }
        else if (Indirect1nr == 2)
        {
            if (USR02551 > 1)
            {
                Temperature_2_ResultFC = (Temperature_2_ResultFC * (1.0 / USR01551)) + (TempFP08 * (1.0 - (1.0 / USR01551)));
            }
        }
        
        if (Indirect1nr == 1) 
        {
            TempFP01 = (Temperature_1_ResultFC - Temperature_1_e0) / (Temperature_1_e1 - Temperature_1_e0);

            if (TempFP01 <= 0) 
            {
                Temperature_1_ResultmA = 4;
            }
            else if (TempFP01 > 0 && TempFP01 <= 1)
            {
                Temperature_1_ResultmA = (TempFP01 * 16) + 4; 
            }
            else if (TempFP01 > 1) 
            {          
                Temperature_1_ResultmA = 20.0;
            }                  
        }
        else if (Indirect1nr == 2)
        {
            TempFP01 = (Temperature_2_ResultFC - Temperature_1_e0) / (Temperature_1_e1 - Temperature_1_e0);

            if (TempFP01 <= 0)
            {
                Temperature_2_ResultmA = 4;
            }
            else if (TempFP01 > 0 && TempFP01 <= 1)
            {
                Temperature_2_ResultmA = (TempFP01 * 16) + 4; 
            }
            else if (TempFP01 > 1)
            {
                Temperature_2_ResultmA = 20;
            }
        }

        Temperature_AVG = (Temperature_1_ResultFC + Temperature_2_ResultFC) / 2;
        Temperature_A_ResultmA = (Temperature_1_mA + Temperature_1_mA) / 2; 

        if (Indirect1nr == 1) 
        {
            if (Temperature_1_ResultFC < Temperature_1_e4 || Temperature_1_ResultFC > Temperature_1_e5) 
            {
                USR01553 = 16;
            }   
        }                
        else if (Indirect1nr == 2)
        {
            if (Temperature_1_ResultFC < Temperature_1_e4 || Temperature_1_ResultFC > Temperature_1_e5)
            {
            USR02553 = 16;
            } 
        }    
        
        if (Indirect1nr == 1)
        {
            if (Temperature_1_ResultFC < Temperature_1_e2)
            {
                USR01453 = 4;
                LS2166 = 36869;
                LS2167 = 2005;
            }
            else if (Temperature_1_ResultFC >Temperature_1_e3) 
            {
                USR01453 = 8;
                LS2166 = 36869;
                LS2167 = 2006;
            }
        }        
        else if (Indirect1nr == 2)
        {
            if (Temperature_2_ResultFC < Temperature_1_e2)
            {
                USR01453 = 4;
                LS2166 = 36869;
                LS2167 = 2005;
            }
            else if (Temperature_2_ResultFC > Temperature_1_e3)
            {
                USR01453 = 8;
                LS2166 = 36869;
                LS2167 = 2006;
            }   
        }

     return (0);
    }

    public double SelectmA1 (int LS2170)
    {   
        double mA_out_1 = 0;

        switch (LS2170) 
        {
            case 1: 
                mA_out_1 = (SG_1_ResultmA * 2968.07); 
            break;

            case 2: 
                mA_out_1 = (Density_1_ResultmA * 2968.07); 
            break;

            case 3:
                mA_out_1 = (TSS_1_ResultmA * 2968.07); 
            break;

            case 4:
                mA_out_1 = (Temperature_1_ResultmA * 2968.07); 
            break;

            case 5: 
                mA_out_1 = (SG_2_ResultmA * 2968.07);
            break;

            case 6: 
                mA_out_1 = (Density_2_ResultmA * 2968.07); 
            break;

            case 7: 
                mA_out_1 = (TSS_2_ResultmA * 2968.07); 
            break;

            case 8: 
                mA_out_1 = (Temperature_2_ResultmA * 2968.07); 
            break;

            case 9: 
                mA_out_1 = (SG_A_ResultmA * 2968.07); 
            break;

            case 10: 
                mA_out_1 = (Density_A_ResultmA * 2968.07); 
            break;

            case 11: 
                mA_out_1 = (TSS_A_ResultmA * 2968.07); 
            break;
            case 12: 
                mA_out_1 = (Temperature_A_ResultmA * 2968.07); 
            break;
        }

        if (LS2170 > 1 || LS2170 < 12)
        {
            mA_out_1 = 4.0 * 2968.07;
        }

        return (mA_out_1);  
    }   
    public double SelectmA2 (int LS2171)
    {   
        double mA_out_1 = 0;

        switch (LS2171) 
        {
            case 1: 
                mA_out_1 = (SG_1_ResultmA * 2968.07); 
            break;

            case 2: 
                mA_out_1 = (Density_1_ResultmA * 2968.07); 
            break;

            case 3:
                mA_out_1 = (TSS_1_ResultmA * 2968.07); 
            break;

            case 4:
                mA_out_1 = (Temperature_1_ResultmA * 2968.07); 
            break;

            case 5: 
                mA_out_1 = (SG_2_ResultmA * 2968.07);
            break;

            case 6: 
                mA_out_1 = (Density_2_ResultmA * 2968.07); 
            break;

            case 7: 
                mA_out_1 = (TSS_2_ResultmA * 2968.07); 
            break;

            case 8: 
                mA_out_1 = (Temperature_2_ResultmA * 2968.07); 
            break;

            case 9: 
                mA_out_1 = (SG_A_ResultmA * 2968.07); 
            break;

            case 10: 
                mA_out_1 = (Density_A_ResultmA * 2968.07); 
            break;

            case 11: 
                mA_out_1 = (TSS_A_ResultmA * 2968.07); 
            break;
            case 12: 
                mA_out_1 = (Temperature_A_ResultmA * 2968.07); 
            break;
        }

        if (LS2171 > 1 || LS2171 < 12)
        {
            mA_out_1 = 4.0 * 2968.07;
        }

        return (mA_out_1);  
    }
    public double SelectmA3 (int LS2172)
    {   
        double mA_out_1 = 0;

        switch (LS2172) 
        {
         case 1: 
                mA_out_1 = (SG_1_ResultmA * 2968.07); 
            break;

            case 2: 
                mA_out_1 = (Density_1_ResultmA * 2968.07); 
            break;

            case 3:
                mA_out_1 = (TSS_1_ResultmA * 2968.07); 
            break;

            case 4:
                mA_out_1 = (Temperature_1_ResultmA * 2968.07); 
            break;

            case 5: 
                mA_out_1 = (SG_2_ResultmA * 2968.07);
            break;

            case 6: 
                mA_out_1 = (Density_2_ResultmA * 2968.07); 
            break;

            case 7: 
                mA_out_1 = (TSS_2_ResultmA * 2968.07); 
            break;

            case 8: 
                mA_out_1 = (Temperature_2_ResultmA * 2968.07); 
            break;

            case 9: 
                mA_out_1 = (SG_A_ResultmA * 2968.07); 
            break;

            case 10: 
                mA_out_1 = (Density_A_ResultmA * 2968.07); 
            break;

            case 11: 
                mA_out_1 = (TSS_A_ResultmA * 2968.07); 
            break;
            case 12: 
                mA_out_1 = (Temperature_A_ResultmA * 2968.07); 
            break;

        }

        if (LS2172 > 1 || LS2172 < 12)
        {
            mA_out_1 = 4.0 * 2968.07;
        }

        return (mA_out_1);  
    }      
    public double SelectmA4 (int LS2173)
    {   
        double mA_out_1 = 0;

        switch (LS2173) 
        {
            case 1: 
                mA_out_1 = (SG_1_ResultmA * 2968.07); 
            break;

            case 2: 
                mA_out_1 = (Density_1_ResultmA * 2968.07); 
            break;

            case 3:
                mA_out_1 = (TSS_1_ResultmA * 2968.07); 
            break;

            case 4:
                mA_out_1 = (Temperature_1_ResultmA * 2968.07); 
            break;

            case 5: 
                mA_out_1 = (SG_2_ResultmA * 2968.07);
            break;

            case 6: 
                mA_out_1 = (Density_2_ResultmA * 2968.07); 
            break;

            case 7: 
                mA_out_1 = (TSS_2_ResultmA * 2968.07); 
            break;

            case 8: 
                mA_out_1 = (Temperature_2_ResultmA * 2968.07); 
            break;

            case 9: 
                mA_out_1 = (SG_A_ResultmA * 2968.07); 
            break;

            case 10: 
                mA_out_1 = (Density_A_ResultmA * 2968.07); 
            break;

            case 11: 
                mA_out_1 = (TSS_A_ResultmA * 2968.07); 
            break;
            case 12: 
                mA_out_1 = (Temperature_A_ResultmA * 2968.07); 
            break;
        }

        if (LS2173 > 1 || LS2173 < 12)
        {
            mA_out_1 = 4.0 * 2968.07;
        }

        return (mA_out_1);  
    }   
    public double SelectmA5 (int LS2174)
    {   
        double mA_out_1 = 0;

        switch (LS2174) 
        {
            case 1: 
                mA_out_1 = (SG_1_ResultmA * 2968.07); 
            break;

            case 2: 
                mA_out_1 = (Density_1_ResultmA * 2968.07); 
            break;

            case 3:
                mA_out_1 = (TSS_1_ResultmA * 2968.07); 
            break;

            case 4:
                mA_out_1 = (Temperature_1_ResultmA * 2968.07); 
            break;

            case 5: 
                mA_out_1 = (SG_2_ResultmA * 2968.07);
            break;

            case 6: 
                mA_out_1 = (Density_2_ResultmA * 2968.07); 
            break;

            case 7: 
                mA_out_1 = (TSS_2_ResultmA * 2968.07); 
            break;

            case 8: 
                mA_out_1 = (Temperature_2_ResultmA * 2968.07); 
            break;

            case 9: 
                mA_out_1 = (SG_A_ResultmA * 2968.07); 
            break;

            case 10: 
                mA_out_1 = (Density_A_ResultmA * 2968.07); 
            break;

            case 11: 
                mA_out_1 = (TSS_A_ResultmA * 2968.07); 
            break;
            case 12: 
                mA_out_1 = (Temperature_A_ResultmA * 2968.07); 
            break;

        }

        if (LS2174 > 1 || LS2174 < 12)
        {
            mA_out_1 = 4.0 * 2968.07;
        }

        return (mA_out_1);  
    }   
    public double SelectmA6 (int LS2175)
    {   
        double mA_out_1 = 0;

        switch (LS2175) 
        {
     case 1: 
                mA_out_1 = (SG_1_ResultmA * 2968.07); 
            break;

            case 2: 
                mA_out_1 = (Density_1_ResultmA * 2968.07); 
            break;

            case 3:
                mA_out_1 = (TSS_1_ResultmA * 2968.07); 
            break;

            case 4:
                mA_out_1 = (Temperature_1_ResultmA * 2968.07); 
            break;

            case 5: 
                mA_out_1 = (SG_2_ResultmA * 2968.07);
            break;

            case 6: 
                mA_out_1 = (Density_2_ResultmA * 2968.07); 
            break;

            case 7: 
                mA_out_1 = (TSS_2_ResultmA * 2968.07); 
            break;

            case 8: 
                mA_out_1 = (Temperature_2_ResultmA * 2968.07); 
            break;

            case 9: 
                mA_out_1 = (SG_A_ResultmA * 2968.07); 
            break;

            case 10: 
                mA_out_1 = (Density_A_ResultmA * 2968.07); 
            break;

            case 11: 
                mA_out_1 = (TSS_A_ResultmA * 2968.07); 
            break;
            case 12: 
                mA_out_1 = (Temperature_A_ResultmA * 2968.07); 
            break;

        }

        if (LS2175 > 1 || LS2175 < 12)
        {
            mA_out_1 = 4.0 * 2968.07;
        }

        return (mA_out_1);  
    }   

}